package net.dp9v.lab1;

import net.dp9v.lab1.wordsAccumulator.Printer;
import net.dp9v.lab1.wordsAccumulator.Processor;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by dpolkovnikov on 07.02.17.
 */
public class Main {
	public static final Logger logger = Logger.getLogger(Main.class);

	static {
		DOMConfigurator.configure("src/net/dp9v/lab1/resources/log4j.xml");
	}

	public static void main(String args[]) {
		Thread            printer = new Thread(new Printer());
		printer.start();
		logger.info("Printer thread start");
		ExecutorService pool = Executors.newFixedThreadPool(10);
		for(String path :
				args) {
			pool.submit(new Processor(path));
			logger.info("Start thread for file: " + path);
		}
		pool.shutdown();
		try {
			pool.awaitTermination(5, TimeUnit.MINUTES);
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		Printer.stop("Finish");
	}

}
