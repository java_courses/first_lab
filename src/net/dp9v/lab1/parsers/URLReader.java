package net.dp9v.lab1.parsers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by dpolkovnikov on 09.02.17.
 * Позволяет читать файл в кодировке UTF-8 в интернете.
 */
public class URLReader extends StreamReader {
	/**
	 * Конструктор
	 *
	 * @param url путь к файлу в интернете
	 * @throws MalformedURLException неверно указан URL
	 * @throws IOException           Ошибка чтения файла
	 */
	public URLReader(String url) throws MalformedURLException, IOException {
		super(new URL(url).openStream());
	}
}
