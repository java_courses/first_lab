package net.dp9v.lab1.parsers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Pattern;

/**
 * Created by dpolkovnikov on 09.02.17.
 * Позволяет читать файл в кодировке UTF-8 из входного потока.
 */
public class StreamReader implements Readable, AutoCloseable {
	private static final String ALLOWED_CHARS  = "1234567890-";
	private static final String FORBIDEN_CHARS = "=+_?!:;.,()—–-\'\"»«";
	private static final String DELIMITERS     = " \n\t";
	private BufferedReader reader;

	/**
	 * Конструктор
	 *
	 * @param is входной поток
	 * @throws IOException ошибка чтения из потока
	 */
	public StreamReader(InputStream is) throws IOException {
		reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
	}

	/**
	 * Удаляет из слова неподдерживаемые символы
	 *
	 * @param s Строка, которую необходимо "очистить"
	 * @return Итоговая строка без неподдерживаемых символов
	 */
	private static String deleteBadSymbols(String s) {
		int leftIndex  = 0;
		int rightIndex = s.length();
		while(leftIndex + 1 != rightIndex && FORBIDEN_CHARS.contains(s.substring(leftIndex, leftIndex + 1))) {
			leftIndex++;
		}
		while(leftIndex != rightIndex && FORBIDEN_CHARS.contains(s.substring(rightIndex - 1, rightIndex))) {
			rightIndex--;
		}
		String res = s.substring(leftIndex, rightIndex);
		return res;
	}

	/**
	 * Закрыть входной поток
	 */
	@Override
	public void close() {
		try {
			reader.close();
		}
		catch (IOException ex) {
			System.out.println("Reader has already closed");
		}
	}

	/**
	 * Чтение следующего слова из входного потока
	 *
	 * @return следующее валидное слово из входного потока. null в случае полного прочтения потока
	 * @throws UnsupportedWordException встречено неподдерживаемое слово
	 * @throws IOException              ошибка чтения слова
	 */
	@Override
	public String readWord() throws UnsupportedWordException, IOException {
		StringBuilder s  = new StringBuilder();
		char          ch = (char) reader.read();

		for(; ; ) {
			while(ch != '\uFFFF' && (!DELIMITERS.contains(ch + "") || s.length() == 0)) {
				if(!DELIMITERS.contains(ch + ""))
					s.append(ch);
				ch = (char) reader.read();
			}
			if(s.length() == 0)
				return null;

			String word = deleteBadSymbols(s.toString().toLowerCase());

			if(isIgnoreWord(word)) {
				s = new StringBuilder();
				continue;
			}
			if(isNormalWord(word)) {
				return word;
			}
			throw new UnsupportedWordException("Найдено неподдерживаемое слово", s.toString());
		}
	}

	/**
	 * Проверяет слово на соответствие паттернам
	 *
	 * @param s слово, которое нужно проверить
	 * @return true - прошло проверку, false - не прошло
	 */
	private boolean isNormalWord(String s) {
		final String REG_EX       = "[0-9]*[а-яё]+[0-9]*";
		final String REG_EX_DEFIS = "[0-9]*[а-яё]+-[а-яё]+[0-9]*";
		return Pattern.matches(REG_EX, s) || Pattern.matches(REG_EX_DEFIS, s);
	}

	private boolean isIgnoreWord(String s) {
		final String NUMBER_REGEX = "[0-9]+";
		return Pattern.matches(NUMBER_REGEX, s) || s.isEmpty();
	}
}
