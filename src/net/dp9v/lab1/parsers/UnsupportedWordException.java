package net.dp9v.lab1.parsers;

import java.io.IOException;

/**
 * Created by dpolkovnikov on 09.02.17.
 * Исключение - встречено неподдерживаемое слово
 */
public class UnsupportedWordException extends IOException {
	private String badWord;

	/**
	 * Консруктор
	 *
	 * @param message сообщение
	 * @param badWord неподдерживаемое слово
	 */
	public UnsupportedWordException(String message, String badWord) {
		super(message);
		this.badWord = badWord;
	}

	/**
	 * Возвращает неподдерживаемое слово
	 *
	 * @return неподдерживаемое слово
	 */
	public String getBadWord() {
		return badWord;
	}
}
