package net.dp9v.lab1.parsers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by dpolkovnikov on 07.02.17.
 * Позволяет читать файл в кодировке UTF-8 на компьютере
 */
public class FileReader extends StreamReader {
	/**
	 * Конструктор
	 *
	 * @param path Путь к файлу на компьютере
	 * @throws FileNotFoundException Файл отсутствует на компьютере
	 * @throws IOException           Ошибка чтения файла
	 */
	public FileReader(String path) throws FileNotFoundException, IOException {
		super(new FileInputStream(path));
	}

}
