package net.dp9v.lab1.parsers;

import java.io.IOException;

/**
 * Created by dpolkovnikov on 07.02.17.
 * Объект поддрживающий чтение слов из входного потока/файла
 */
public interface Readable {
	String readWord() throws IOException;
}
