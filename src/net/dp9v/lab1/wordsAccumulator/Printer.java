package net.dp9v.lab1.wordsAccumulator;


import org.apache.log4j.Logger;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by dpolkovnikov on 10.02.17.
 */
public class Printer implements Runnable {
	private static          ConcurrentLinkedQueue<String> outputStrings = new ConcurrentLinkedQueue<>();
	private volatile static boolean                       stopMarker    = false;
	private static          Logger                        logger        = Logger.getLogger(Printer.class);


	/**
	 * Остановка работы потока печати
	 *
	 * @param message сообщение выводимое перед остановкой потока
	 */
	public static void stop(String message) {
		if(!stopMarker) {
			outputStrings.add(message);
			stopMarker = true;
			logger.info("Printer get stopped signal");
		}
	}

	/**
	 * Добавление строки в очередь печати
	 *
	 * @param s строка для добавления в очередь
	 */
	public static void addString(String s) {
		if(!stopMarker)
			outputStrings.offer(s);
	}

	/**
	 * Печать следующей в очереди строки и удаление её из очереди
	 */
	private static void printString() {
		System.out.println(outputStrings.poll());
	}

	/**
	 * Возвращает следующую строку в очереди на печать (для теста)
	 *
	 * @return первая строка в очередь на печать
	 */
	public static String nextWord() {
		if(outputStrings.isEmpty())
			return null;
		return outputStrings.poll();
	}

	/**
	 * Сбрасывает очередь печати и устанавливает stopMarker = false
	 */
	public static void reset() {
		outputStrings.clear();
		stopMarker = false;
	}

	/**
	 * Реализация метода run() из интерфейса Runnable
	 */
	@Override
	public void run() {
		while(!stopMarker || outputStrings.size() > 0) {
			if(outputStrings.size() > 0)
				printString();
		}
		logger.info("Printer finished");
	}
}
