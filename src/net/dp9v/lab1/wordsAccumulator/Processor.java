package net.dp9v.lab1.wordsAccumulator;

import net.dp9v.lab1.parsers.FileReader;
import net.dp9v.lab1.parsers.StreamReader;
import net.dp9v.lab1.parsers.URLReader;
import net.dp9v.lab1.parsers.UnsupportedWordException;
import org.apache.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;

/**
 * Created by dpolkovnikov on 11.02.17.
 * Статический класс для последовательной печати строк из нескольких потоков
 */
public class Processor implements Runnable {
	private static volatile boolean                  stopFlag = false;
	private static          HashMap<String, Integer> words    = new HashMap<>();
	private static          Logger                   logger   = Logger.getLogger(Processor.class);


	private String path;

	/**
	 * Конструктор класса
	 *
	 * @param path путь к файлу для чтения
	 */
	public Processor(String path) {
		this.path = path;
		stopFlag = false;
	}

	/**
	 * Добавляет строку в очередь на вывод
	 *
	 * @param word строка для добавления
	 * @return возвращает false, в случае если ранее был вызван метод stop()
	 */
	private static synchronized void addWord(String word) {
		if(words.containsKey(word))
			words.put(word, words.get(word) + 1);
		else
			words.put(word, 1);
		printMap();
	}

	/**
	 * Добавляет текущее состояние словаря words в очередь печати
	 */
	private static synchronized void printMap() {
		StringBuilder sb = new StringBuilder("-------------\n");
		for(HashMap.Entry<String, Integer> word :
				words.entrySet()) {
			sb.append(word.getKey() + ": " + word.getValue() + "\n");
		}
		String s = sb.toString();
		Printer.addString(s);
	}

	/**
	 * Останавливает выполнения потока и запрещает добавление строк в очередь печати
	 *
	 * @param message сообщение для добавления в очередь печати
	 */
	private static synchronized void stop(String message) {
		if(!stopFlag) {
			stopFlag = true;
			Printer.stop(message);
			logger.warn("Processor treads get stop signal with message \n" + message);
		}
	}

	/**
	 * Реализация метода run() из интерфейса Runnable
	 */
	@Override
	public void run() {
		try (StreamReader r = path.toLowerCase().startsWith("http") ?
				new URLReader(path) :
				new FileReader(path)) {
			String word = null;
			while((word = r.readWord()) != null) {
				if(stopFlag)
					break;
				addWord(word);
			}
			logger.info("File \"" + path + "\" was finished. Thread closed.");
		}
		catch (FileNotFoundException ex) {
			stop("Файл не найден:\n" + path + "\n");
		}
		catch (MalformedURLException ex) {
			stop("Ошибка ввода URL:\n" + path + "\n");
		}
		catch (UnsupportedWordException ex) {
			stop("В файле встречено неподдерживаемое слово:\nИмя файла:" +
			     path + "\nСлово: " + ex.getBadWord() + "\n");
		}
		catch (IOException ex) {
			stop("Ошибка открытия файла:\n" + path + "\n");
		}
	}

}
