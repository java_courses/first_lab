package net.dp9v.lab1.tests;

import net.dp9v.lab1.wordsAccumulator.Printer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by dpolkovnikov on 14.02.17.
 */
class PrinterTest {

	@BeforeEach
	void setUp() {
		Printer.reset();
	}

	@Test
	void stop() {
		Printer.addString("test");
		Printer.stop("Test2");
		Printer.stop("Bad_word");
		assertEquals("test", Printer.nextWord());
		assertEquals("Test2", Printer.nextWord());
		assertNull(Printer.nextWord());
	}


	@Test
	void addString() {
		for(String word :
				Constants.CORRECT_STRINGS) {
			Printer.addString(word);
		}
		for(String word :
				Constants.CORRECT_STRINGS) {
			assertEquals(word, Printer.nextWord());
		}
	}

}