package net.dp9v.lab1.tests;

import net.dp9v.lab1.parsers.StreamReader;
import net.dp9v.lab1.parsers.UnsupportedWordException;
import org.junit.jupiter.api.*;

import java.io.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;


/**
 * Created by dpolkovnikov on 12.02.17.
 */
class StreamReaderTest {
	private InputStream CF1, WF1, WF2, WF3;

	@BeforeEach
	void setUp() throws FileNotFoundException {
		CF1 = new FileInputStream(Constants.CORRECT_FILE_1);
		WF1 = new FileInputStream(Constants.WRONG_FILE_1);
		WF2 = new FileInputStream(Constants.WRONG_FILE_2);
		WF3 = new FileInputStream(Constants.WRONG_FILE_3);
	}

	@AfterEach
	void tearDown() throws IOException {
		CF1.close();
		WF1.close();
		WF2.close();
		WF3.close();
	}

	@Test
	void readWord() throws IOException {

		StreamReader srs = new StreamReader(CF1);
		for(String word :
				Constants.CORRECT_STRINGS) {
			assertEquals(word, srs.readWord());
		}
		assertNull(srs.readWord());

		UnsupportedWordException ex = assertThrows(UnsupportedWordException.class, () -> {
			StreamReader sr = new StreamReader(WF1);
			sr.readWord();
		});
		assertEquals("тест12тее", ex.getBadWord());

		ex = assertThrows(UnsupportedWordException.class, () -> {
			StreamReader sr = new StreamReader(WF2);
			sr.readWord();
		});
		assertEquals("exeption", ex.getBadWord());

		ex = assertThrows(UnsupportedWordException.class, () -> {
			StreamReader sr = new StreamReader(WF3);
			sr.readWord();
		});
		assertEquals("lol*wat", ex.getBadWord());
	}

}