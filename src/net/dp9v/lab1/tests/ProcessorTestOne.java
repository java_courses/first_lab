package net.dp9v.lab1.tests;

import net.dp9v.lab1.wordsAccumulator.Printer;
import net.dp9v.lab1.wordsAccumulator.Processor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by dpolkovnikov on 14.02.17.
 */
class ProcessorTestOne {

	@AfterEach
	void setUp() {
		Printer.reset();
	}

	@Test
	public void testCorrectFileRead() throws InterruptedException {
		Thread th = new Thread(new Processor(Constants.CORRECT_FILE_2));
		th.start();
		th.join();
		for(String s :
				Constants.EXPECTED_STRINGS) {
			assertEquals(s, Printer.nextWord());
		}
	}

	@Test
	public void testFileReadNonExistedFile() throws InterruptedException {
		Thread th = new Thread(new Processor(Constants.NONEXISTENT_FILE));
		th.start();
		th.join();
		assertEquals("Файл не найден:\n" + Constants.NONEXISTENT_FILE + "\n", Printer.nextWord());
	}

	@Test
	public void testFileReadNonExistedFileWeb() throws InterruptedException {
		Thread th = new Thread(new Processor(Constants.NONEXISTENT_FILE_WEB));
		th.start();
		th.join();
		assertEquals("Ошибка открытия файла:\n" + Constants.NONEXISTENT_FILE_WEB + "\n", Printer.nextWord());
	}

	@Test
	public void testReadBadURL() throws InterruptedException {
		Thread th = new Thread(new Processor(Constants.BAD_URL_WEB));
		th.start();
		th.join();
		assertEquals("Ошибка ввода URL:\n" + Constants.BAD_URL_WEB + "\n", Printer.nextWord());
	}

	@Test
	public void testReadWrongFile() throws InterruptedException {
		Thread th = new Thread(new Processor(Constants.WRONG_FILE_1));
		th.start();
		th.join();
		assertEquals("В файле встречено неподдерживаемое слово:\nИмя файла:" + Constants.WRONG_FILE_1 +
		             "\nСлово: тест12тее\n", Printer.nextWord());
		assertNull(Printer.nextWord());
	}

}