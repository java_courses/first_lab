package net.dp9v.lab1.tests;

import net.dp9v.lab1.parsers.FileReader;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by dpolkovnikov on 14.02.17.
 */
class FileReaderTest {


	@Test
	void openTest() throws IOException {
		FileReader CF1 = new FileReader(Constants.CORRECT_FILE_1);
		for(String word :
				Constants.CORRECT_STRINGS) {
			assertEquals(word, CF1.readWord());
		}
		assertNull(CF1.readWord());
		assertThrows(FileNotFoundException.class, () -> {FileReader tmp = new FileReader(Constants.NONEXISTENT_FILE);});
	}

}