package net.dp9v.lab1.tests;

import net.dp9v.lab1.parsers.FileReader;
import net.dp9v.lab1.parsers.URLReader;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by dpolkovnikov on 14.02.17.
 */
class URLReaderTest {

	@Test
	void openTest() throws IOException {
		URLReader CF1 = new URLReader(Constants.CORRECT_FILE_WEB);
		for(String word :
				Constants.CORRECT_STRINGS) {
			assertEquals(word, CF1.readWord());
		}
		assertNull(CF1.readWord());
		assertThrows(IOException.class,
		             () -> {URLReader tmp = new URLReader(Constants.NONEXISTENT_FILE_WEB);});
		assertThrows(MalformedURLException.class,
		             () -> {URLReader tmp = new URLReader(Constants.BAD_URL_WEB);});
	}
}