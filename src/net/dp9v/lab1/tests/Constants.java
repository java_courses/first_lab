package net.dp9v.lab1.tests;

/**
 * Created by dpolkovnikov on 14.02.17.
 */
public class Constants {
	final static String   CORRECT_FILE_1           = "src/net/dp9v/lab1/tests/files/good1.txt";
	final static String   CORRECT_FILE_2           = "src/net/dp9v/lab1/tests/files/good2.txt";
	final static String   WRONG_FILE_1             = "src/net/dp9v/lab1/tests/files/unsupport1.txt";
	final static String   WRONG_FILE_2             = "src/net/dp9v/lab1/tests/files/unsupport2.txt";
	final static String   WRONG_FILE_3             = "src/net/dp9v/lab1/tests/files/unsupport3.txt";
	final static String   NONEXISTENT_FILE         = "src/net/dp9v/lab1/tests/files/nonexistent.txt";
	final static String   CORRECT_FILE_WEB         = "https://gitlab.com/java_courses/first_lab/raw/master/src/net/dp9v/lab1/tests/files/good1.txt";
	final static String   BAD_URL_WEB              = "httpss: // gitlab.com/ java_courses/first_lab/raw/master/src/net/dp9v/lab1/tests/files/src/net/dp9v/lab1/tests/files/unsupport1.txt";
	final static String   NONEXISTENT_FILE_WEB     = "https://gitlab.com/java_courses/first_lab/nonexistent.txt";
	final static String[] CORRECT_STRINGS          = {"мой", "дядя", "самых", "честных", "131правил",
	                                                  "когда-то", "чуть1231", "12331не1231"};
	final static int      EXPECTED_COUNT           = 23;
	final static String   EXPECTED_LAST_STRING_BAD = "В файле встречено неподдерживаемое слово:\n" +
	                                                 "Имя файла:src/net/dp9v/lab1/tests/files/unsupport2.txt\n" +
	                                                 "Слово: exeption\n";

	final static String[] EXPECTED_STRINGS         = {
			"-------------\n" +
			"мой: 1\n",
			"-------------\n" +
			"мой: 1\n" +
			"дядя: 1\n",
			"-------------\n" +
			"мой: 1\n" +
			"дядя: 2\n",
			"-------------\n" +
			"мой: 2\n" +
			"дядя: 2\n",
			"-------------\n" +
			"мой: 2\n" +
			"который: 1\n" +
			"дядя: 2\n",
			"-------------\n" +
			"мой: 2\n" +
			"который: 1\n" +
			"самых: 1\n" +
			"дядя: 2\n",
			"-------------\n" +
			"честных: 1\n" +
			"мой: 2\n" +
			"который: 1\n" +
			"самых: 1\n" +
			"дядя: 2\n",
			"-------------\n" +
			"честных: 1\n" +
			"мой: 2\n" +
			"и: 1\n" +
			"который: 1\n" +
			"самых: 1\n" +
			"дядя: 2\n",
			"-------------\n" +
			"честных: 1\n" +
			"бил: 1\n" +
			"мой: 2\n" +
			"и: 1\n" +
			"который: 1\n" +
			"самых: 1\n" +
			"дядя: 2\n",
			"-------------\n" +
			"честных: 1\n" +
			"бил: 1\n" +
			"мой: 2\n" +
			"и: 2\n" +
			"который: 1\n" +
			"самых: 1\n" +
			"дядя: 2\n",
			"-------------\n" +
			"честных: 1\n" +
			"бил: 1\n" +
			"грабил: 1\n" +
			"мой: 2\n" +
			"и: 2\n" +
			"который: 1\n" +
			"самых: 1\n" +
			"дядя: 2\n",
			"-------------\n" +
			"честных: 1\n" +
			"бил: 1\n" +
			"грабил: 1\n" +
			"мой: 2\n" +
			"и: 3\n" +
			"который: 1\n" +
			"самых: 1\n" +
			"дядя: 2\n",
			"-------------\n" +
			"заставлял: 1\n" +
			"честных: 1\n" +
			"бил: 1\n" +
			"грабил: 1\n" +
			"мой: 2\n" +
			"и: 3\n" +
			"который: 1\n" +
			"самых: 1\n" +
			"дядя: 2\n",
			"-------------\n" +
			"заставлял: 1\n" +
			"честных: 1\n" +
			"бил: 1\n" +
			"грабил: 1\n" +
			"уважать: 1\n" +
			"мой: 2\n" +
			"и: 3\n" +
			"который: 1\n" +
			"самых: 1\n" +
			"дядя: 2\n",
			"-------------\n" +
			"заставлял: 1\n" +
			"честных: 1\n" +
			"бил: 1\n" +
			"грабил: 1\n" +
			"уважать: 1\n" +
			"мой: 2\n" +
			"и: 3\n" +
			"который: 1\n" +
			"себя: 1\n" +
			"самых: 1\n" +
			"дядя: 2\n"};
}
