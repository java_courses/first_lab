package net.dp9v.lab1.tests;

import net.dp9v.lab1.parsers.StreamReader;
import net.dp9v.lab1.wordsAccumulator.Printer;
import net.dp9v.lab1.wordsAccumulator.Processor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by dpolkovnikov on 14.02.17.
 */
class ProcessorTestMultyThread {
	@BeforeEach
	void setUp() {
		Printer.reset();
	}

	@Test
	void testCorrectFileRead() throws InterruptedException {
		Thread th1 = new Thread(new Processor(Constants.CORRECT_FILE_1));
		Thread th2 = new Thread(new Processor(Constants.CORRECT_FILE_2));
		th1.start();
		th2.start();
		th1.join();
		th2.join();
		String tmp_s;
		int    count = 0;
		while((tmp_s = Printer.nextWord()) != null) {
			count++;
		}
		assertEquals(Constants.EXPECTED_COUNT, count);
	}

	@Test
	void testReadWrongFile() throws InterruptedException {
		Thread th1 = new Thread(new Processor(Constants.CORRECT_FILE_1));
		Thread th2 = new Thread(new Processor(Constants.WRONG_FILE_2));
		th2.start();
		th1.start();
		th1.join();
		th2.join();
		String s     = "";
		String tmp_s;
		int    count = 0;
		while((tmp_s = Printer.nextWord()) != null) {
			count++;
			s = tmp_s;
		}
		assertEquals(s, Constants.EXPECTED_LAST_STRING_BAD);
		assertNull(Printer.nextWord());
	}

}